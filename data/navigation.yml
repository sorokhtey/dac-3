---
prodnames:
  jiracloud: "JIRA Cloud platform"
  jsdcloud: "JIRA Service Desk Cloud"
  jswcloud: "JIRA Software Cloud"
  confcloud: "Confluence Cloud"
  bbcloud: "Bitbucket Cloud"
  connect: "Connect"
"Navigation":
- product:
  title: "Jira Cloud platform"
  name: "jiracloud"
  url: "/cloud/jira/platform/"
  categories:
    - category:
      title: "Guides"
      name: "devguide"
      url: "/cloud/jira/platform/"
      indexLinkTitle: "Latest updates"
      subcategories:
      - subcategory:
        title: "Introduction and basics"
        name: "intro"
        expandAlways: true
        items:
          - title: "Integrating with JIRA Cloud"
            url: "/cloud/jira/platform/integrating-with-jira-cloud"
            # Replaced jira-cloud-development-platform.md with integrating-with-jira-cloud.md
            # DAC-195 consolidate the following two pages 
            # https://developer.atlassian.com/static/connect/docs/latest/guides/introduction.html
            # https://developer.atlassian.com/jiracloud/jira-cloud-development-platform-39981102.html
            #core
          - title: "Getting started"
            url: "/cloud/jira/platform/getting-started"
            # DAC-196 consolidate the above page with
            # https://developer.atlassian.com/static/connect/docs/latest/guides/development-setup.html
            #core
          - title: "Architecture overview"
            url: "/cloud/jira/platform/architecture-overview"
            # DAC-197 rework architecture overview content
            # https://developer.atlassian.com/static/connect/docs/latest/guides/introduction.html
            #core
          - title: "Frameworks and tools"
            url: "/cloud/jira/platform/frameworks-and-tools"
            # see DAC-197 
            # https://developer.atlassian.com/static/connect/docs/latest/developing/frameworks-and-tools.html
            #core
      - subcategory:
        title: "Security"
        name: "security"
        items:
          - title: "Security overview"
            url: "/cloud/jira/platform/security-overview"
            # DAC-198 consolidate the following two pages
            # https://developer.atlassian.com/static/connect/docs/latest/concepts/security.html
            # https://developer.atlassian.com/display/jiracloud/Authentication+and+authorization
          - title: "Authentication for add-ons"
            url: "/cloud/jira/platform/authentication-for-add-ons"
            # https://developer.atlassian.com/static/connect/docs/latest/concepts/authentication.html
          - title: "Understanding JWT for add-ons"
            url: "/cloud/jira/platform/understanding-jwt"
            # https://developer.atlassian.com/static/connect/docs/latest/concepts/understanding-jwt.html
          - title: "OAuth 2.0 bearer tokens for add-ons"
            url: "/cloud/jira/platform/oauth-2-jwt-bearer-token-authorization-grant-type"
          - title: "OAuth for REST APIs"
            url: "/cloud/jira/platform/jira-rest-api-oauth-authentication"
          - title: "Basic auth for REST APIs"
            url: "/cloud/jira/platform/jira-rest-api-basic-authentication"
          - title: "Cookie-based auth for REST APIs"
            url: "/cloud/jira/platform/jira-rest-api-cookie-based-authentication"
      - subcategory:
        title: "Learning"
        name: "guides"
        expandAlways: true
        items:
          - title: "Patterns and examples"
            url: "/cloud/jira/platform/patterns-and-examples/"
          - title: "Tutorials and guides"
            id: "tutorialsAndGuides"
            url: "/cloud/jira/platform/tutorials-and-guides/"
      - subcategory: 
        title: "Building blocks"
        name: "blocks"
        expandAlways: true
        items:
          - title: "Add-on descriptor"
            url: '/cloud/jira/platform/add-on-descriptor'  
          - title: "Conditions"
            url: '/cloud/jira/platform/conditions'
          - title: "Context parameters"
            url: '/cloud/jira/platform/context-parameters'
          - title: "Entity properties"
            url: '/cloud/jira/platform/jira-entity-properties'
          - title: "Webhooks"
            url: '/cloud/jira/platform/webhooks'
          - title: "Scopes"
            url: '/cloud/jira/platform/scopes'
          - title: "Hosted Data Storage"
            url: '/cloud/jira/platform/hosted-data-storage'
      - subcategory:
        title: "Other considerations"
        name: "other"
        items:
          - title: "Atlassian Design Guidelines"
            url: "https://design.atlassian.com/"
          - title: "Atlassian UI library"
            url: "https://docs.atlassian.com/aui/"
          - title: "Developing for Marketplace"
            url: "https://developer.atlassian.com/market/developing-for-the-marketplace"

          - title: "Public licensing"
            url: "https://developer.atlassian.com/market/add-on-licensing-for-developers"
    - category:
      title: "Reference"
      name: "reference"
      url: "/cloud/jira/platform/jira-cloud-platform-rest-api"
      subcategories:
      - subcategory:
        title: "REST APIS"
        name: "rest"
        expandAlways: true
        items:
          - title: "About JIRA Cloud platform REST API"
            url: "/cloud/jira/platform/jira-cloud-platform-rest-api"
            #core
          - title: "JIRA Cloud platform REST API"
            url: "https://docs.atlassian.com/jira/REST/cloud/"
            #core
          - title: "JIRA platform REST API scopes"
            url: "/cloud/jira/platform/jira-rest-api-scopes"
            # https://developer.atlassian.com/static/connect/docs/latest/scopes/jira-rest-scopes.html
            #core
      - subcategory:
        title: "Modules"
        name: "modules"
        items:
          - title: "About JIRA Platform modules"
            url: "/cloud/jira/platform/about-jira-modules"
            # core
          - title: "Dashboard item"
            url: "#"
            # core
          - title: "Dialog"
            url: "#"
            # core
          - title: "Entity property"
            url: "#"
            # core
          - title: "Global permission"
            url: "#"
            # core
          - title: "Issue field"
            url: "#"
            # core
          - title: "Page"
            url: "#"
            # core
          - title: "Project admin tab panel"
            url: "#"
            # core
          - title: "Project permission"
            url: "#"
            # core
          - title: "Report"
            url: "#"
            # core
          - title: "Search request view"
            url: "#"
            # core
          - title: "Tab panel"
            url: "#"
            # core
          - title: "Web item"
            url: "#"
            # core
          - title: "Web panel"
            url: "#"
            # core
          - title: "Web section"
            url: "#"
            # core
          - title: "Workflow post function"
            url: "#"
          - title: "Extension points for the admin console"
            url: "/cloud/jira/platform/extension-points-for-the-admin-console"
          - title: "Extension points for project configuration"
            url: "/cloud/jira/platform/extension-points-for-project-configuration"
          - title: "Extension points for the end-user UI"
            url: "/cloud/jira/platform/extension-points-for-the-end-user-ui"
          - title: "Extension points for the 'View issue' page"
            url: "/cloud/jira/platform/extension-points-for-the-view-issue-page"
      - subcategory:
        title: "JavaScript API"
        name: "jsapi"
        items:
          - title: "About the JavaScript API"
            url: "/cloud/jira/platform/about-the-javascript-api"
            # https://developer.atlassian.com/static/connect/docs/latest/concepts/javascript-api.html
            # core
          - title: "AP"
            url: "#"
            # core
          - title: "Confluence"
            url: "#"
            # core
          - title: "Cookie"
            url: "#"
            # core
          - title: "Custom-content"
            url: "#"
            # core
          - title: "Dialog"
            url: "#"
            # core
          - title: "Events"
            url: "#"
            # core
          - title: "History"
            url: "#"
            # core
          - title: "Inline-dialog"
            url: "#"
            # core
          - title: "JIRA"
            url: "#"
            # note this is Jira in the old nav
            # core
          - title: "Messages"
            url: "#"
            # core
          - title: "Navigator"
            url: "#"
            # core
          - title: "Request"
            url: "#"
            # core
          - title: "User"
            url: "#"
            # core
    - category:
      title: "Get help"
      name: "help"
      url: "/cloud/jira/platform/get-help"
- product:
  title: "Jira Software Cloud"
  name: "jswcloud"
  url: "/cloud/jira/software/"
  categories:
    - category:
      title: "Guides"
      name: "devguide"
      url: "/cloud/jira/software/"
      subcategories:
      - subcategory:
        title: "Introduction and basics"
        name: "intro"
        expandAlways: true
        items:
          - title: "Integrating with JIRA Software Cloud"
            url: "/cloud/jira/software"
          - title: "Getting started"
            url: "/cloud/jira/software/getting-started"
            #core    
          - title: "Architecture overview"
            url: "/cloud/jira/software/architecture-overview"
            #core
          - title: "Frameworks and tools"
            url: "/cloud/jira/software/frameworks-and-tools"
            #core                 
      - subcategory:
        title: "Security"
        name: "security"
        items:
          - title: "Security overview"
            url: "/cloud/jira/software/security-overview"
            #core
          - title: "Authentication for add-ons"
            url: "/cloud/jira/software/authentication-for-add-ons"
            #core
          - title: "Understanding JWT for add-ons"
            url: "/cloud/jira/software/understanding-jwt"
            #core
          - title: "OAuth 2.0 bearer tokens for add-ons"
            url: "/cloud/jira/software/oauth-2-jwt-bearer-token-authorization-grant-type"
            #core
          - title: "OAuth for REST APIs"
            url: "/cloud/jira/software/jira-rest-api-oauth-authentication"
            #core
          - title: "Basic auth for REST APIs"
            url: "/cloud/jira/software/jira-rest-api-basic-authentication"
            #core
          - title: "Cookie-based auth for REST APIs"
            url: "/cloud/jira/software/jira-rest-api-cookie-based-authentication"
            #core
      - subcategory:
        title: "Learning"
        name: "guides"
        expandAlways: true
        items:
          - title: "Patterns and examples"
            url: "/cloud/jira/software/patterns-and-examples/"
          - title: "Tutorials and guides"
            id: "tutorialsAndGuides"
            url: "/cloud/jira/software/tutorials-and-guides/"
      - subcategory:
        title: "Building blocks"
        name: "blocks"
        expandAlways: true
        items:
          - title: "Add-on descriptor"
            url: '/cloud/jira/software/add-on-descriptor'  
          - title: "Conditions"
            url: '/cloud/jira/software/conditions'
          - title: "Context parameters"
            url: '/cloud/jira/software/context-parameters'
          - title: "Entity properties"
            url: '/cloud/jira/software/jira-entity-properties'
          - title: "Webhooks"
            url: '/cloud/jira/software/webhooks'  
      - subcategory:
        title: "Other considerations"
        name: "other"
        items:
          - title: "Atlassian Design Guidelines"
            url: "https://design.atlassian.com/"
          - title: "Atlassian UI library"
            url: "https://docs.atlassian.com/aui/"
          - title: "Developing for Marketplace"
            url: "https://developer.atlassian.com/market/developing-for-the-marketplace"
          - title: "Public licensing"
            url: "https://developer.atlassian.com/market/add-on-licensing-for-developers"
    - category:
      title: "Reference"
      name: "reference"
      url: "/cloud/jira/software/jira-software-cloud-rest-api"
      subcategories:
      - subcategory:
        title: "REST APIS"
        name: "rest"
        expandAlways: true
        items:
          - title: "About the JIRA Software Cloud REST API"
            url: "/cloud/jira/software/jira-software-cloud-rest-api"
          - title: "JIRA Software Cloud REST API"
            url: "https://docs.atlassian.com/jira-software/REST/cloud/"
          - title: "JIRA Cloud platform REST API"
            url: "https://docs.atlassian.com/jira/REST/cloud/"
          - title: "JIRA Software REST API scopes"
            url: "/cloud/jira/software/jira-software-rest-api-scopes"
          - title: "JIRA platform REST API scopes"
            url: "/cloud/jira/software/jira-rest-api-scopes"
      - subcategory:
        title: "Modules"
        name: "modules"
        items:
          - title: "About JIRA modules"
            url: "/cloud/jira/software/about-jira-modules"
            # core
          - title: "Boards"
            url: "/cloud/jira/software/boards"
          - title: "Dashboard item"
            url: "#"
            # core
          - title: "Dialog"
            url: "#"
            # core
          - title: "Entity property"
            url: "#"
            # core
          - title: "Global permission"
            url: "#"
            # core
          - title: "Issue field"
            url: "#"
            # core
          - title: "Page"
            url: "#"
            # core
          - title: "Project admin tab panel"
            url: "#"
            # core
          - title: "Project permission"
            url: "#"
            # core
          - title: "Report"
            url: "#"
            # core
          - title: "Search request view"
            url: "#"
            # core
          - title: "Tab panel"
            url: "#"
            # core
          - title: "Web item"
            url: "#"
            # core
          - title: "Web panel"
            url: "#"
            # core
          - title: "Web Section"
            url: "#"
            # core
          - title: "Workflow post function"
            url: "#"
          - title: "Extension points for the admin console"
            url: "/cloud/jira/software/extension-points-for-the-admin-console"
            # core
          - title: "Extension points for project configuration"
            url: "/cloud/jira/software/extension-points-for-project-configuration"
            # core
          - title: "Extension points for the end-user UI"
            url: "/cloud/jira/software/extension-points-for-the-end-user-ui"
            # core
          - title: "Extension points for the 'View issue' page"
            url: "/cloud/jira/software/extension-points-for-the-view-issue-page"
      - subcategory:
        title: "JavaScript API"
        name: "jsapi"
        items:
          - title: "About the JavaScript API"
            url: "/cloud/jira/software/about-the-javascript-api"
            # https://developer.atlassian.com/static/connect/docs/latest/concepts/javascript-api.html
            # core
          - title: "AP"
            url: "#"
            # core
          - title: "Confluence"
            url: "#"
            # core
          - title: "Cookie"
            url: "#"
            # core
          - title: "Custom-content"
            url: "#"
            # core
          - title: "Dialog"
            url: "#"
            # core
          - title: "Events"
            url: "#"
            # core
          - title: "History"
            url: "#"
            # core
          - title: "Inline-dialog"
            url: "#"
            # core
          - title: "JIRA"
            url: "#"
            # note this is Jira in the old nav
            # core
          - title: "Messages"
            url: "#"
            # core
          - title: "Navigator"
            url: "#"
            # core
          - title: "Request"
            url: "#"
            # core
          - title: "User"
            url: "#"
            # core
    - category:
      title: "Get help"
      name: "help"
      url: "/cloud/jira/software/get-help"
- product:
  title: "Jira Service Desk Cloud"
  name: "jsdcloud"
  url: "/cloud/jira/service-desk/"
  categories:
    - category:
      title: "Guides"
      name: "devguide"
      url: "/cloud/jira/service-desk/"
      subcategories:
      - subcategory:
        title: "Introduction and basics"
        name: "intro"
        expandAlways: true
        items:
          - title: "Integrating with JIRA Service Desk Cloud"
            url: "/cloud/jira/service-desk"
          - title: "Getting started"
            url: "/cloud/jira/service-desk/getting-started"
            #core
          - title: "Architecture overview"
            url: "/cloud/jira/service-desk/architecture-overview"
            #core
          - title: "Frameworks and tools"
            url: "/cloud/jira/service-desk/frameworks-and-tools"
            #core
      - subcategory:
        title: "Security"
        name: "security"
        items:
          - title: "Security overview"
            url: "/cloud/jira/service-desk/security-overview"
            #core
          - title: "Authentication for add-ons"
            url: "/cloud/jira/service-desk/authentication-for-add-ons"
            #core
          - title: "Understanding JWT for add-ons"
            url: "/cloud/jira/service-desk/understanding-jwt"
            #core
          - title: "OAuth 2.0 bearer tokens for add-ons"
            url: "/cloud/jira/service-desk/oauth-2-jwt-bearer-token-authorization-grant-type"
            #core
          - title: "OAuth for REST APIs"
            url: "/cloud/jira/service-desk/jira-rest-api-oauth-authentication"
            #core
          - title: "Basic auth for REST APIs"
            url: "/cloud/jira/service-desk/jira-rest-api-basic-authentication"
            #core
          - title: "Cookie-based auth for REST APIs"
            url: "/cloud/jira/service-desk/jira-rest-api-cookie-based-authentication"
            #core
      - subcategory:
        title: "Learning"
        name: "guides"
        expandAlways: true
        items:
          - title: "Patterns and examples"
            url: "/cloud/jira/service-desk/patterns-and-examples/"
          - title: "Tutorials and guides"
            id: "tutorialsAndGuides"
            url: "/cloud/jira/service-desk/tutorials-and-guides/"
      - subcategory:
        title: "Building blocks"
        name: "blocks"
        expandAlways: true
        items:
          - title: "Add-on descriptor"
            url: '/cloud/jira/service-desk/add-on-descriptor' 
            #core         
          - title: "Conditions"
            url: "/cloud/jira/service-desk/conditions"
            #core
                      
            #core
          - title: "Context parameters"
            url: "/cloud/jira/service-desk/context-parameters"
            #core
          - title: "Entity properties"
            url: "/cloud/jira/service-desk/jira-entity-properties"
          - title: "Hosted Data Storage"
            url: '/cloud/jira/service-desk/hosted-data-storage'
          - title: "Scopes"
            url: '/cloud/jira/service-desk/scopes'
          - title: "Webhooks"
            url: '/cloud/jira/service-desk/webhooks'
          - title: "JIRA Service Desk webhooks"
            url: "/cloud/jira/service-desk/jira-service-desk-webhooks"
          - title: "Scopes"
            url: '/cloud/jira/service-desk/scopes'
          - title: "Hosted Data Storage"
            url: '/cloud/jira/service-desk/hosted-data-storage'
      - subcategory:
        title: "Other considerations"
        name: "other"
        items:
          - title: "Atlassian Design Guidelines"
            url: "https://design.atlassian.com/"
          - title: "Atlassian UI library"
            url: "https://docs.atlassian.com/aui/"
          - title: "Developing for Marketplace"
            url: "https://developer.atlassian.com/market/developing-for-the-marketplace"
          - title: "Public licensing"
            url: "https://developer.atlassian.com/market/add-on-licensing-for-developers"
    - category:
      title: "Reference"
      name: "reference"
      url: "/cloud/jira/service-desk/jira-service-desk-cloud-rest-api"
      subcategories:
      - subcategory:
        title: "REST APIS"
        name: "rest"
        expandAlways: true
        items:
          - title: "About JIRA Service Desk Cloud REST API"
            url: "/cloud/jira/service-desk/jira-service-desk-cloud-rest-api"
          - title: "JIRA Service Desk Cloud REST API"
            url: "https://docs.atlassian.com/jira-servicedesk/REST/cloud/"
          - title: "JIRA Cloud platform REST API"
            url: "https://docs.atlassian.com/jira/REST/cloud/"
          - title: "JIRA Service Desk REST API scopes"
            url: "/cloud/jira/service-desk/jira-service-desk-rest-api-scopes"
          - title: "JIRA platform REST API scopes"
            url: "/cloud/jira/service-desk/jira-rest-api-scopes"
      - subcategory:
        title: "Modules"
        name: "modules"
        items:
          - title: "About JIRA modules"
            url: "/cloud/jira/service-desk/about-jira-modules"
            #core
          - title: "Agent view"
            url: "/cloud/jira/service-desk/agent-view"
          - title: "Automation action"
            url: "/cloud/jira/service-desk/automation-action"
          - title: "Customer portal"
            url: "/cloud/jira/service-desk/customer-portal"
          - title: "Dashboard item"
            url: "#"
            # core
          - title: "Dialog"
            url: "#"
            # core
          - title: "Entity property"
            url: "#"
            # core
          - title: "Global permission"
            url: "#"
            # core
          - title: "Issue field"
            url: "#"
            # core
          - title: "Page"
            url: "#"
            # core
          - title: "Portal icon"
            url: "/cloud/jira/service-desk/portal-icon"
          - title: "Project admin tab panel"
            url: "#"
            # core
          - title: "Project permission"
            url: "#"
            # core
          - title: "Report"
            url: "#"
            # core
          - title: "Search request view"
            url: "#"
            # core
          - title: "Tab panel"
            url: "#"
            # core
          - title: "Web item"
            url: "#"
            # core

          - title: "Web item target"
            url: "/cloud/jira/service-desk/web-item-target"

          - title: "Web panel"
            url: "#"
            # core
          - title: "Web Section"
            url: "#"
            # core
          - title: "Workflow post function"
            url: "#"
          - title: "Extension points for the admin console"
            url: "/cloud/jira/service-desk/extension-points-for-the-admin-console"
            # core
          - title: "Extension points for project configuration"
            url: "/cloud/jira/service-desk/extension-points-for-project-configuration"
            # core
          - title: "Extension points for the end-user UI"
            url: "/cloud/jira/service-desk/extension-points-for-the-end-user-ui"
            # core
          - title: "Extension points for the 'View issue' page"
            url: "/cloud/jira/service-desk/extension-points-for-the-view-issue-page"
      - subcategory:
        title: "JavaScript API"
        name: "jsapi"
        items:
          - title: "About the JavaScript API"
            url: "/cloud/jira/service-desk/about-the-javascript-api"
            # https://developer.atlassian.com/static/connect/docs/latest/concepts/javascript-api.html
            # core
          - title: "AP"
            url: "#"
            # core
          - title: "Confluence"
            url: "#"
            # core
          - title: "Cookie"
            url: "#"
            # core
          - title: "Custom-content"
            url: "#"
            # core
          - title: "Dialog"
            url: "#"
            # core
          - title: "Events"
            url: "#"
            # core
          - title: "History"
            url: "#"
            # core
          - title: "Inline-dialog"
            url: "#"
            # core
          - title: "JIRA"
            url: "#"
            # note this is Jira in the old nav
            # core
          - title: "Messages"
            url: "#"
            # core
          - title: "Navigator"
            url: "#"
            # core
          - title: "Request"
            url: "#"
            # core
          - title: "User"
            url: "#"
            # core
    - category:
      title: "Get help"
      name: "help"
      url: "/cloud/jira/service-desk/get-help"
