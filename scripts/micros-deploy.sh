#!/bin/bash

set -ex

if [ $# -ne 1 ]; then
    echo "Supply an environment to deploy to"
    exit -1
fi

export ENV=$1
export DAC_IMAGE_VERSION=`cat release.txt`   # Saved during build phase

micros service:deploy \
    dac-static \
    -f  micros/service-descriptor.yaml \
    -e $ENV
