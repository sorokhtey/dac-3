#!/usr/bin/env python
import os
import re
import argparse
import glob
import time
import requests
from junit_xml import TestCase, TestSuite

"""
Generate report on links under folder and prefix

Run as:

time ./scripts/check-links.py -p /jiracloud -o link_report.xml ./public/jiracloud/

"""

def extract_valid_links(content):
    """ Extracts all HREF links from string """
    result = []
    urls = re.findall(r'href=(?!&quot;)[\'"]?([^\'" >]+)', content)
    for u in urls:
        result.append(u)
    return result

def get_head_for_url(original_file, l):
     try:
         status_code = requests.head(l, allow_redirects=True).status_code
         if status_code != 200:
             print('\t[%s] %s' % (status_code, l))
         return status_code
     except requests.exceptions.ConnectionError as e:
         print('\t[ERROR] Connection error: %s' % l)
         print('--- \n %s \n ---' % e)
         return 'E'
     except ConnectionRefusedError as e:
         print('\t[ERROR] Connection refused: %s' % l)
         print('--- \n %s \n ---' % e)
         return 'E'
     except requests.packages.urllib3.exceptions.NewConnectionError as e:
         print('\t[ERROR] New Connection Error: %s' % l)
         print('--- \n %s \n ---' % e)
         return 'E'
     except requests.packages.urllib3.exceptions.MaxRetryError as e:
         print('\t[ERROR] Max Retry Error : %s' % l)
         print('--- \n %s \n ---' % e)
         return 'E'
     except requests.exceptions.MissingSchema as e2:
         if original_file.count('index.md') > 0 :
             asset = os.path.join(original_file[:-len('index.html')],l)
             if os.path.exists(asset):
                 #print('\t[OK][RELATIVE] %s' % l)
                 return 'OK'
             else:
                 print('\t[KO][MISSING SCHEMA] %s' % asset)
                 print('\t[ERROR] Missing schema: %s' % e2)
                 return 'KO'
         else:
             print('\t[ERROR] Missing schema: %s' % l)
             print('--- \n %s \n ---' % e2)
             return 'E'

def relative_link_exists(src, l):
    """ Joins root `src` folder and relative url `l` and check if it exists
        on the local file system.
    """
    on_fs_name = os.path.splitext(os.path.join(src, l[1:]))[0] + '.html'
    if os.path.exists(on_fs_name) or os.path.exists(os.path.join(src, l[1:])):
        #print('\t[OK][RELATIVE] %s' % on_fs_name)
        return 'OK'
    else:
        print('\t[KO][RELATIVE] %s' % l)
        return 'KO'

def check_links(f, links, src, already_checked, ignore_list = {}, prefix="", test_suites=[]):
    """ Validate all `links`. If the the link is relative it checks that a
        matching file exists on the file system.
    """

    # Manipulate root folder if there's overlap with URL prefix
    test_cases = []
    if prefix:
        if src.count(prefix) > 0:
            src = src[:len(prefix) - 1]
    for l in links:
        if any(re.match(r_str, l) for r_str in ignore_list):
            print('Ignoring link:', l)
            continue
        if l not in already_checked:
            if l.count(src) > 0 or l.startswith('/'):
                already_checked[l] = relative_link_exists(src, l)
            elif l.startswith('#'):
                already_checked[l] = 'ANCHOR'
            else:
                time.sleep(0.2)
                already_checked[l] = get_head_for_url(f, l)

            if already_checked[l] not in (200,'ANCHOR', 'OK'):
                tc = TestCase('Link: %s' % l , '', 0, '', '')
                tc.add_failure_info('Status code: %s' % already_checked[l])
            else:
                tc = TestCase('Link: %s' % l , '', 0, '', '')
            test_cases.append(tc)
        else:
            #print('\t[%s][DUPE] %s' % (already_checked[l], l))
            pass
    test_suites.append(TestSuite("Checking: %s" % f, test_cases))



def check_links_in_tree(src, ignore_list={}, prefix=""):
    """ Check links in documents under folder in `src`

        It will ignore links in `ignore_list` group
        It will match relative links to the filesystem using `src` and `prefix`,
        for example:
            src = '/public/jiracloud'
            prefix = '/jiracloud'
            test_link = '/jiracloud/somedocument.html'
        The checker will check that '/jiracloud/somedocument.html' exists.
    """

    already_checked = {}
    tree = os.path.join(src, '**/*.html')
    test_suites = []
    uniq_files = {}
    print('Starting directory:', tree)
    for f in glob.glob(tree , recursive=True):
        if not f in uniq_files:
            if os.path.isfile(f):
                links = extract_valid_links(open(f).read())
                if links:
                    print(f)
                    check_links(f, links, src, already_checked, ignore_list=ignore_list, prefix=prefix, test_suites=test_suites)
            else:
                print('Skipping folder:', f)
            uniq_files[f] = True
    return TestSuite.to_xml_string(test_suites)

def get_options():
    """ Parse command line arguments """
    parser = argparse.ArgumentParser(description='Checks for valid links in a folder tree')
    parser.add_argument('target', metavar='target', help='Static folder tree to link check')
    parser.add_argument('-p',
                        '--prefix',
                        metavar='prefix',
                        help='verbose: specify relative url prefix')
    parser.add_argument('-o',
                        '--output',
                        metavar='output',
                        help='verbose: file to write report to')
    return parser.parse_args()

if __name__ == '__main__':
    OPT = get_options()
    ignore_list = { 'https://BASE_URL.atlassian.net': 1,
                    'http://localhost:8090/jira/rest/api/2/issue/HSP-1': 1,
                    'http://localhost:2990': 1,
                    'https://bitbucket.org/.*': 1,
                    'http://www.myremoteapplication.com/webhookreceiver': 1,
                    '/about/*': 1,
                    '/jiradev/*': 1}
    if OPT.target:
        links_report = check_links_in_tree(OPT.target,
            ignore_list=ignore_list,
            prefix=OPT.prefix)
        if OPT.output:
            f = open(OPT.output, 'w')
            f.write(links_report)
