---
title: "Using Stash and JIRA for development bliss"
date: "2014-12-17"
author: "tpettersen"
categories: ["git"]
---

<style>
  .video-player, .screen-caps, .big-image {
    display: block;
    margin: 25px auto;
  }

  .video-player {
    width: 560px;
  }

  .screen-caps img {
    width: 32%;
  }

</style>

A couple of weeks ago I ran a webinar on how to enhance your Git development 
experience with JIRA and Stash. The half-hour allocated for Q&A wasn't quite 
long enough, so I'm following up on some of the unanswered questions in blog 
form. 

If you didn't catch the webinar, it's now
[live on YouTube](https://www.youtube.com/watch?v=M-u8-Ga6if0):

<div class="video-player">
<iframe width="560" height="315" src="//www.youtube.com/embed/M-u8-Ga6if0" frameborder="0" allowfullscreen></iframe>  
</div>

Check it out if you're interested in:

- viewing related commits, branches, pull requests and builds from the JIRA 
issue page
- automatically transitioning issues when branches or pull requests are updated
- a sneak preview of the buttery-smooth Git release workflows available in 
the JIRA 6.4 EAP (early access program)

<div class="screen-caps">
  <a href="auto_issue_transitions.png"><img alt="Auto issue transitions" src="auto_issue_transitions.png"></a>
  <a href="create_branch.png"><img alt="Branch creation" src="create_branch.png"></a>
  <a href="releasing_is_hard.png"><img alt="Releasing is hard" src="releasing_is_hard.png"></a>
</div>

For those who attended, thanks for the great questions! Here are the answers to 
some of the ones that I missed during the session:

**Can you customize which JIRA issue state to transition to when a branch is 
updated in Stash?** *(Lucy C)*

Yes. The JIRA/Stash integration makes no assumption about the structure of 
your JIRA workflow. Trigger configuration is built into the 
[JIRA workflow editor](https://confluence.atlassian.com/display/JIRA/Configuring+workflow+triggers#Configuringworkflowtriggers-Guide:settinguptriggers)
so you can choose which repository events to listen to, and which transitions to 
trigger, based on your own workflow requirements. 

**Does the new release screen warn you if your release branch contains code 
references issues that are NOT assigned to the release?** *(Agustín M B)*

Not *yet*. The release screen has been built in an extensible manner though, 
and warnings based on branches and assigned fix versions are a logical extension 
of what we've built so far. It's likely that you'll see these and other new 
release warnings in future releases of JIRA.

**Can I get a copy of the example development workflow that you 
demonstrated?** *(Thomas R)*

Sure thing! The workflow I demonstrated is a simple development workflow 
with separate states for review, testing, and waiting for deployment. Workflow 
triggers are bound to a couple of different transitions:

- if a *branch is created* while the issue is **Open** it will transition to 
**In Progress**
- if a *pull request is created* while the issue is **In Progress** it will 
transition to **In Review**
- if a *pull request is merged* while the issue is **In Review** or **Ready 
for Testing** it will transition to **Ready for Deployment**

<img alt="Simple JIRA workflow" src="simple_workflow.png" class="big-image">

The workflow is available as a 
[JIRA Workflow Bundle](Simple-Development-Workflow.jwb) or as 
[XML](Simple-Development-Workflow.xml). 

**Does JIRA have an API I can use to replicate the Stash integration for 
other repository hosting solutions?** *(Hangsu M)*

Yes and no. Both JIRA and Stash are built on top of a 
[rich plugin system](https://developer.atlassian.com/stash) that allows 
extension. An external developer theoretically could extend JIRA to have 
similar integration with third-party repository hosts, but it would be a 
non-trivial amount of effort to develop and maintain such an integration. 
Practically speaking, you're likely to get the best JIRA and Git experience by 
using JIRA and Stash together.

**How are tests integrated to automatically run?** *(Josh L)*

My colleague [Steve](https://twitter.com/tarkasteve) recently gave a great webinar on 
[Stash & Continuous Integration](https://www.youtube.com/watch?v=wSASqmzBaAs) 
which answers your question in more depth, but briefly:

1. A developer creates or updates a branch in Stash.
2. Stash notifies your CI server that the repository has been updated.
3. Your CI server builds the changes and posts the results to Stash.
4. (Optionally) Stash will prevent the branch from being merged into ``master`` 
until there is a successful build.
5. JIRA indexes the branch, build and pull request information and displays it 
on the corresponding issue page.

**If I am a current JIRA user, can I import my existing Git repositories into 
Stash?** *(Alex G)*

For sure! Since Git is a DVCS, migrating to a different Git hosting system is 
ridiculously easy. All you need to do is push your repository to Stash, and 
your linked JIRA will begin indexing your commit, branch, and pull request 
metadata automatically.

**Where does that Progress button live again?** *(Ryan M)*

To preview the new project progress view, you'll need to install the 
[JIRA 6.4 EAP](https://confluence.atlassian.com/display/JIRA/EAP+Releases) and 
start your JIRA server with the following flag:

    -Djira.plugin.devstatus.phasefour.enabled=true

The Progress link will appear on the 
[left hand menu on your project view](https://www.youtube.com/watch?v=M-u8-Ga6if0#t=1246).

**When you create a feature branch, is a build plan for the branch 
automatically created in Bamboo?** *(Trey S)*

Yes! Bamboo's [automatic plan-branching](https://confluence.atlassian.com/display/BAMBOO/Using+plan+branches#Usingplanbranches-Autobranching) 
handles this for you. The [Stash & Continuous Integration](https://www.youtube.com/watch?v=wSASqmzBaAs) webinar I mentioned above has more details.

**We are currently using a Git repository to store our issues. If we want to 
move to Stash/JIRA, what do we need to do in order to integrate the 
existing data?** *(Yair H)*

JIRA has 
[built-in importers](https://confluence.atlassian.com/display/JIRA/Migrating+from+Other+Issue+Trackers) 
for various competing issue trackers. There's also a 
[CSV importer](https://confluence.atlassian.com/display/JIRA/Importing+Data+from+CSV) 
you can fall back on if there's no explicit support for your current issue 
tracker.

**Does JIRA support deleting feature branches in Stash after they are merged?** 
*(Andrew S)*

Stash will offer to clean up your branch for you when you merge your pull 
request.

**Can I have Stash with the cloud versions of JIRA and Bamboo?** *(Luciano P)*

Yes, provided your Stash server is publicly addressable so that JIRA and 
Bamboo can reach it.

**Is it possible to distribute Git commit hooks with Stash?** *(Tom M)*

The most convenient way that we've found to distribute client-side Git hooks 
is to commit them to the repository and then symlink them to your 
``.git/hooks`` directory. Stash also has a 
[Repository Hooks API](https://developer.atlassian.com/stash/docs/latest/how-tos/repository-hooks.html) 
for implementing server-side hooks.

**In Stash, is there a way to prevent "ninja" commits directly to master?** 
*(Jamieson E)*

Yes! Stash 
[branch permissions](https://confluence.atlassian.com/display/STASH/Using+branch+permissions) 
allow you to prevent direct updates to specific branches or branch patterns.

**How does Stash associate a pull request with an issue?** *(Mike K)*

A pull request will be associated with an issue if it: 

- contains a commit that references the issue key in it's commit message;
- contains the issue key in it's title; or
- contains the issue key in it's source branch.

See [Understanding triggers](https://confluence.atlassian.com/display/JIRA/Configuring+workflow+triggers#Configuringworkflowtriggers-understandtriggersUnderstandingtriggers) 
for a deeper explanation of how this works.

**How are you structuring the relationship between feature branches and builds 
such that you see when builds fail for a given issue?** *(Reuben K)*

Stash knows which commits relate to which issues and which commits have failing 
builds so long as you include the JIRA issue key in the commit message. If the most recent commit that references a particular issue has a 
failing build, then that issue is considered to have a failing build.

**How does Stash handle merge conflicts?** *(Jesse T)*

If there is a conflict, Stash will display the conflicting changes in the UI 
and prompt you to resolve them using your IDE.

**What happens if the pull request is rejected?** *(Tomas M)*

It's up to you :) There is a "Pull Request Rejected" event that you can bind 
to whatever issue transition makes sense for your teams workflow.

**Did you tell us that JIRA and Stash love each other and *then* say that 
they're brother and sister?** *(Michael K)*

Err, *yes*. Platonic, familial love. Though JIRA and Stash get along better 
than some of the siblings I know.

<img alt="JIRA and Stash" src="jira-and-stash.png" class="big-image">

If you have further questions, feedback, or ideas on what you'd like to see 
in future webinars, please ping me on Twitter - I'm 
[@kannonboy](https://twitter.com/kannonboy) - or leave a comment below. If you 
want to hear more about Git, developer tools and what's going on in Atlassian 
engineering, you can subscribe to our blog 
[here](https://developer.atlassian.com/blog/feed.xml).