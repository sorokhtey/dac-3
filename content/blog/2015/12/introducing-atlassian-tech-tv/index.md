---
title: "Introducing Atlassian Tech TV"
date: "2015-12-17T10:45:00+07:00"
author: "cmountford"
categories: ["video", "Developer experience", "Opinion", "Interview" ]
lede: "Atlassian Tech TV  is a new project that takes you inside Atlassian
where you get to hear how we make software directly from members of Atlassian's
software teams. We kick off with a series of one-on-one interviews as I chat
with some of our most talented people from roles across engineering, design,
QA, and product management." 
---

For my tenth year working at Atlassian, I've changed from doing 100% hands-on
software development to focus on writing, speaking and, most recently, video
production. It's a freaky change for me - shooting and editing video is quite
unlike software development but I love change. This project gives me an
opportunity to showcase the knowledge and insights of the talented
Atlassians I work with every day. It's also an opportunity to bring my
engineering methods and tools into the video production workflow, but that's a
topic for another time.

<a href="https://www.youtube.com/channel/UCwbjXgNKd7KlzCDip_YqleQ">Atlassian
Tech TV</a> is a new project that takes you inside Atlassian where you get to
hear how we make software directly from members of Atlassian's software teams.
We kick off with a series of one-on-one interviews as I chat with some of our
most talented people from roles across engineering, design, QA, and product
management.

We publish to our dedicated <a href="https://www.youtube.com/channel/UCwbjXgNKd7KlzCDip_YqleQ">Atlassian Tech
TV YouTube channel</a> weekly, so subscribe now to catch episodes as soon as
they're available. 

To get the flavor of the content of these interviews, check out the trailer:

<center style="margin-top:18px;"><iframe width="560" height="315" src="https://www.youtube.com/embed/dAeFk8AkY6s" frameborder="0" allowfullscreen></iframe></center>

### Reaching depth

From each interview I take at least one five minute topical selection and cut a
preview video. People are busy and can't always commit time to watch the whole
interview. But it's impossible to actually reach any depth or subtlety on
innovative topics in only five minutes, and after a shoot I end up with more
than 30 minutes of great discussion, so I also make the full interviews
available, broken into two digestible parts. 

We aim to bring a diverse range of people from different roles and levels of
experience, each with a unique perspective and valuable insights on how
Atlassian makes products. I want our subscribers to get something useful out of
every episode, something to take back to their teams and hopefully, every now
and then, a new way of looking at things that brings a breakthrough change of
thinking. 

In my first interview, I sit down with <a href="https://twitter.com/niick">Nick
Pellow</a>, development manager for 
<a href="https://bitbucket.org/product/server">Bitbucket Server</a> 
(previously known as Stash), to discuss topics such as the technique of
dogfooding and the importance of root cause analysis for bugs. We also learn a
little about how Nick got into software. As a teenager he was earmarked by his
father for solving the 
<a href="https://en.wikipedia.org/wiki/Blinking_twelve_problem ">infamous
blinking twelve problem</a> on the family VCR years before expanding his
technical passion into computer science at university. Check out what Nick
Pellow has to say about eating one's own dog food here:

<center style="margin-top:18px;"><iframe width="560" height="315" src="https://www.youtube.com/embed/e9_TYsjBlkU" frameborder="0" allowfullscreen></iframe></center>

Where Nick's experience led us into topics around people leadership, my
interview with software architect <a href="https://twitter.com/robbieg8s">Robbie 
Gates</a> features an interesting discussion on _technical_ leadership. Robbie
and I discuss cloud-native architectures and we also delve into some high level
technical topics such as functional programming (FP), even touching lightly on
its mathematical basis in category theory, a branch of algebra that is Robbie
specialized in during his previous life in academia. Here's the five minute
preview episode of my interview with Robbie:

<center style="margin-top:18px;"><iframe width="560" height="315" src="https://www.youtube.com/embed/HSQ9ET0bOYg" frameborder="0" allowfullscreen></iframe></center>

Both Nick's and Robbie's interviews have been very popular, sparking
conversations online and generating a growing amount of positive feedback.
People are hungry for the inside story of how Atlassian makes software.  

### What's to come

Things are off to a great start with six videos published so far but more than
anything I'm excited for what's to come. I have about ten more interviews in my
edit queue and top of that list is my interview with front-end developer 
<a href="https://twitter.com/lucykbain">Lucy Bain</a>. 

Lucy moved from the USA to a place she'd never visited and professed to know
almost nothing about: Sydney, Australia. Taking a job at Atlassian, she joined
the Bitbucket Server product team. In previous roles she worked as a full-stack
developer with a specific interest in Ruby. Lucy shares her experience in agile
methods and takes a very practical approach to the benefits of practices like
pair programming. Check out 
<a href="https://developer.atlassian.com/blog/2015/05/try-pair-programming/">Lucy's 
blog on Pair Programming</a> 
from earlier this year.

Other episodes coming down the pipe include my chat with principal engineer Jed
Wesley-Smith, who, in addition to providing architectural guidance on back-end
implementation details, is a great supporter of our developer communities as he
volunteers to run many of the meetup events we host at our offices, making sure
the beer is cold.

For insights from my conversations with Atlassians, 
<a href="https://www.youtube.com/channel/UCwbjXgNKd7KlzCDip_YqleQ">subscribe to
Atlassian Tech TV</a>, and if there's an Atlassian you'd especially like me to
interview, or if you have feedback about the shows, hit me up on twitter: 
<a href="https://twitter.com/chromosundrift">@chromosundrift</a>

