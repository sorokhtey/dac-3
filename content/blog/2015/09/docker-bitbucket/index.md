---
title: "Integrating Docker Hub 2.0 into Bitbucket with Atlassian Connect"
date: "2015-09-17T06:00:00+07:00"
author: "ssmith"
categories: ["docker", "bitbucket", "atlassian-connect", "clojure"]
---

<style>
  .float-image {
      display: block; 
      margin: 15px auto 30px auto;
      box-shadow: 10px 10px 15px #888888;
  }
</style>


As part of the [Docker Hub 2.0 launch][hub2] we're pleased to announce
integration of Docker Hub into Atlassian Bitbucket. This brings your Docker
workflow together with Bitbucket to save you time and allow you to see source
code stats along side your Docker repo in one place.

[hub2]: http://blog.docker.com/2015/09/docker-hub-2-0/


## What it does

<img class="float-image" src="screenshot.png" />

The Docker Hub shows the status of a corresponding Docker Hub image repository
from within your Bitbucket repository. This allows you to keep your Dockerfiles
in Bitbucket and see their build, star and pull status directly in the
repository.

Installing it is a 2-step process.

1. Go to your account settings in Bitbucket, select "Find new add-ons" and click
   "Install" on "Docker Hub Integration".
1. As not all repositories necessarily have Dockerfiles the add-on is disabled
   by default. To enable it for a repository add the file
   `.docker-repository.yml` in the root.


By default the add-on will assume that your Bitbucket and Docker Hub
accounts/repositories have the same names; e.g. if your Bitbucket respository
is:

    https://bitbucket.org/ssmith/mydocker

the corresponding Hub repository is:

    https://hub.docker.com/ssmith/mydocker

You can override this default in `.docker-repository.yml` [YAML][YAML] file. If
you add the entry `repository` the add-on will use this for the Docker
account/repository. For example:

    repository: tarkasteve/multipy

The integration is fairly simple at the moment, but we intend to improve it over
time, including adding support for private repositories and registries, manual
build actions, and more build information.


[YAML]: https://en.wikipedia.org/wiki/YAML


## Technical details

The add-on is built on top of Atlassian's
[Atlassian Connect for Bitbucket][bb-connect] framework. This system gives
remotely hosted, third-party services access to the UI real-estate and internal
events of Bitbucket, without having to become part of Bitbucket. It does this by
leveraging modern web technologies such as [webhooks][webhooks], [REST][REST]
and [cross-domain messaging][xdm].

Because it is built on web-technologies this means it is by definition
cross-platform. As a developer this makes me very happy, as it allows me to
build Atlassian add-ons in whatever language or framework I wish. In this case
the Docker Hub add-on is built on top of the Clojure language. There'll be
another blog post along shortly to explain some of the details about this, but
for now the add-on is open-source and [hosted on Bitbucket][plugin-repo], so feel
free to have a dig around it there if you're interested.

For hosting the add-on is built into a [12-Factor][12factor] application in a
Docker container, and deployed to our own hosting service called "Micros", that
runs on top of AWS. But Connect add-on can be hosted anywhere, and I'll be
talking a bit about how to do this with various cloud solutions in future posts.

[bb-connect]: https://developer.atlassian.com/bitbucket/
[webhooks]: https://en.wikipedia.org/wiki/Webhook
[REST]: https://en.wikipedia.org/wiki/Representational_state_transfer
[xdm]: https://en.wikipedia.org/wiki/Web_Messaging
[12factor]: http://12factor.net/
[plugin-repo]: https://bitbucket.org/ssmith/bitbucket-docker-connect
