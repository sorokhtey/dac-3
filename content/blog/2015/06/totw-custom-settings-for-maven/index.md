---
title: "Tip of the Week: Set up global settings for Maven"
date: "2015-06-02T15:00:00+07:00"
author: "jgarcia"
categories: ["Maven", "Bamboo","totw"]
---

This week, we're highlighting a tip from [Jim
Bethancourt](https://twitter.com/jimbethancourt) of [Triple Point
Technologies](http://www.tpt.com) about configuring Maven in your Bamboo remote
agents. If you use multiple agents on the same machine, this neat trick will
ensure the agents have a consistent configuration each time out.

>Do you have multiple agents on a single machine and use a custom settings.xml
in your Maven builds?  Make things easier on yourself!

\- Jim Bethancourt

## Pre-populate your settings.xml files

When Maven starts, it looks for a generic set of settings in either `%HOME%\.m2`
(Windows) or `$HOME/.m2` (Linux). To make sure multiple agents and
build plans start with the same Maven settings, make sure you have configured
this file appropriately for your test environment. More info about the shape and
features of this file can be found here: [Maven
Settings](https://maven.apache.org/settings.html).

Let us know what
you think in the comments!

Watch out for news and info from our
[@AtlassianDev](https://www.twitter.com/atlassiandev) Twitter feed!

Follow me at [@bitbucketeer](https://www.twitter.com/bitbucketeer)!
