---
title: "Tip of the week: stow those logs"
date: "2015-06-30T16:00:00+07:00"
author: "jgarcia"
categories: ["totw", "devops"]
---

A new tip of the week comes to us from [Todd](https://twitter.com/toddjkarl),
who wants to remind us that valuable log files can be obliterated during some
events. Be sure to keep them safe!

Todd says it best:

>Sometimes incidents get resolved without us really understanding the root
cause, and sometimes this is because one or more recovery steps (say rolling
back to a snapshot) obliterates a necessary JIRA log file that we could have
used in root-cause analysis. Consider porting your JIRA log into a tool like
Splunk. It has helped me avoid that sinking feeling when you realize your logs
are gone (a few times!).

An answer to this is found at [Atlassian
Answers](https://answers.atlassian.com/questions/323782/answers/3015908),
including step-by-step instructions, but we're interested in hearing your best strategy in the comments. See you next week!

Watch out for news and info from our
[@AtlassianDev](https://www.twitter.com/atlassiandev) Twitter feed!

Follow me at [@bitbucketeer](https://www.twitter.com/bitbucketeer)!
