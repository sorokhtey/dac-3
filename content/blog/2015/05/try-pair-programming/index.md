---
title: "Try pair programming"
date: "2015-05-21T16:00:00+07:00"
author: "lbain"
categories: ["developer experience", "teams"]
---

<style type="text/css">
.aui-page-panel-inner .recommendation {
  width: 70%;
  background: #f5f5f5;
  margin: 2em auto;
  padding: 0.5em 1em;
  border: 1px solid #cccccc;
  border-radius: 5px;
}

.aui-page-panel-inner .image {
  max-width: 50%;
  margin: 2em auto;
  display: block;
}

.lede {
  display: none;
  visibility: hidden;
}

.blog-archive .lede {
  display: block;
  visibility: visible;
}

</style>

<p class="lede">Pair programming is great for getting started on a code base and an excellent way to
get to know your teammates better. But if your team doesn't pair regularly you might not know how to
get started. If you've never paired before, give it a try and see how you like it!</p>

<div class="image">
  <img alt="Better together!" src="images/intro.jpg">
</div>

## Why pair?
**Share knowledge**: you'll learn, teach, and work with code you might have missed. Often you understand the code better at the end of a pairing session becuase each of you were asking "why?" more than when you work alone.

**Good introduction**: pairing can be particularly useful for new hires. It's a great way to get to know your team, learn about coding styles and expectations, and find who's the right person to ask about a given topic.

**Stay focused**: checking your social media site of choice is much less appealing when there's a person right there to talk with.

**Better code**: theoretically you don't write code much faster with two people, but you write it with fewer bugs. There isn't a lot of research to back this up. Personally, I think the other reasons are more compelling.

#### Aren't these the same benefits we give for code review?
You caught me! [Pair programming is like code review](http://blog.codinghorror.com/pair-programming-vs-code-reviews/). However, pair programming has a major advantage over code review: you review in real time.

## Wow, this sounds amazing! How can I get started?

This article is about starting to pair in a culture that doesn't do a lot (if any) pairing now. Some of the advice given will seem odd to people who pair regularly. My hope is that this gives a good starting point and introduction to pairing. Then people can choose to do more research into pairing and change their style to better fit them and their team.

#### Who should I pair with?
Anyone who wants (or is willing) to pair with you! A willing, happy pair is better than the "best matched" pair.

<p class="newbie recommendation">
Pair with a few (5+) people before you decide if pairing is for you.
</p>

People don't want you to waste your time by working on their task. Push back on this politeness, if it's worth their time to do it, it's worth you time to pair on it (not always true, but generally the case).

<p class="newbie recommendation">
For the first few pairing sessions, go to your pair. This will build up good karma before you ask them to come to you.
</p>

Some people don't love pairing, be willing to gracefully accept a "no."

#### How do I find a pair?
Start by asking. No success? Send calendar invites to people. (People take you more seriously if you've got a super official calendar entry for your pairing session.)

<p class="newbie recommendation">
Send calendar invites to everyone on your team. Set up regular pairing sessions to learn, teach, and get caught up.
</p>

#### How long should I pair for?
People do everything from 90 min sessions to pairing all day, every day for a full sprint. One team found [many short pairings was better](http://csis.pace.edu/~grossman/dcs/XR4-PromiscuousPairing.pdf) (YMMV). Check with your pair on how long they want the pairing session to last.

<p class="newbie recommendation">
Start with 1.5 to 2 hour sessions and build up from there.
</p>


<div class="image">
  <img alt="Stop before the pairing devolves" src="images/how-long.jpg">
</div>


#### How often should I pair?
It's between you, your pair, and your team; just be sure that you're all on the same page.

<p class="newbie recommendation">
If possible, begin with two sessions a week with different people on your team. That way you'll see what it's like to pair with different people.
</p>

## What do I do now?

#### Quick vocabulary
**Driver**: types, bounces ideas around; gets the micro view of the code

**Navigator**: looks for logic problems, bugs, and better implementations, acts as a sounding board, and thinks ahead to potential problems; gets the macro view of the code


<div class="image">
  <img alt="Driver vs. Navigator" src="images/pairing-vocab.jpg">
</div>

<p class="newbie recommendation">
Think of the navigator as the code reviewer. Constantly be on the look out for better solutions.
</p>

#### You've got a few options for the pairing part:
Note: The names below are entirely my own invention, they're not official pair programming vocabulary.

##### The classic

<div class="image">
  <img alt="The classic" src="images/the-classic.jpg">
</div>

Bring your keyboard and mouse, keep swapping the "driver" role with your pair. People usually swap every 20 minutes to hour; for beginners I'd err on the side of too short.

* Pros: you get to contribute as you go along.
* Cons: limited desk space for extra keyboard and mouse.

My team has recently set up a pairing station. It has two keyboards, mice, and monitors &ndash; plug and play pairing!

##### The lazy

<div class="image">
  <img alt="The lazy" src="images/the-lazy.jpg">
</div>

Same as above, but don't bring your keyboard or mouse.

* Pros: won't be playing mouse stealing games, starting to pair isn't a big hassle.
* Cons: have to move your arm to point at the screen (heaven forbid!).

##### The noob

<div class="image">
  <img alt="The noob" src="images/the-noob.jpg">
</div>

Be the navigator the whole time.

* Pros: not as stressful, more opportunity to ask questions.
* Cons: not as satisfying, harder to stay focused.

I love the noob. It opens opportunities to ask questions, learn how the team does things, and learn what your pair is particularly good at. Excellent for new hires like me!

##### The distracted

<div class="image">
  <img alt="The distracted" src="images/the-distracted.jpg">
</div>

Like the noob, but you bring your laptop with you. Keep your laptop closed most of the time, only use it to check syntax, google solutions, or settle a debate. Don't disengage from your pair for more than a few minutes.

* Pros: you can look up things.
* Cons: very easy to get distracted!

<p class="newbie recommendation">
Start with the noob and fall back to the distracted when you need to look something up. Build up to the classic.
</p>

#### Any other tips or tricks?
Have good hygiene

<div class="image">
  <img alt="Make sure you smell nice" src="images/hygiene.jpg">
</div>

<p class="pairing recommendation">
Make sure you've showered, put on deodorant, brushed your teeth, eaten a mint, and skipped the garlic.

</p>

Be inclusive

<div class="image">
  <img alt="Always welcome your pair" src="images/welcoming-pairing.png">
</div>

<p class="pairing recommendation">
Talk a lot, seek first to understand, and make your pair feel welcome.
</p>



### I tried it and I loved/hated it!
Yay! Thanks for giving it a go. I'd love to hear about your experiences!

If you hated it then I'm sorry to hear that. Did you try pairing with a few different people? If not, give it a couple more tries. Not everyone likes pairing, but I think it's something you have to try first to know.

#### Images from...
* [1minus1.com](http://1minus1.com/develop)
* [zenpayroll.com](https://zenpayroll.com/blog/zenpayroll-is-not-hiring-hackers/)
* [c1.staticflickr.com](https://c1.staticflickr.com/5/4068/4638056301_f34564ce78_b.jpg)
* [commons.wikimedia.org](http://commons.wikimedia.org/wiki/File:Pair_Programming.jpg)
* [twicsy.com](http://twicsy.com/i/6br4Sd)
