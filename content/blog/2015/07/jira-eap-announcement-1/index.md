---
title: "JIRA 7 EAP release now available"
date: "2015-07-28T15:00:00+07:00"
author: "dmeyer"
categories: ["JIRA","EAP"]
---

The JIRA development team is proud to announce that the first Early Access Program (EAP)
 release of JIRA 7 is now  available. JIRA 7 is the biggest JIRA release ever. There are 
 a large number of API changes and library changes, so now is the time to begin testing
 your add-on. 
[Download the JIRA 7 EAP today](https://www.atlassian.com/software/jira/download-eap?utm_source=DAC&utm_medium=blog&utm_campaign=jira-eap-announcement) and check out the
[preparing for JIRA 7 development guide](https://developer.atlassian.com/x/PYCkAQ?utm_source=DAC&utm_medium=blog&utm_campaign=jira-eap-announcement).

<img style="width:60%;display:block;margin:16px auto" src="jira-7.png">

###What's the deal with EAPs?
[EAP releases](https://confluence.atlassian.com/display/JIRA/EAP+Releases?utm_source=DAC&utm_medium=blog&utm_campaign=jira-eap-announcement) are early previews of JIRA during development. 
We release these previews so that Atlassian's add-on developers can see new features as they are developed and test their add-ons for compatibility with new APIs.

The number of EAP releases before the final build varies depending on the nature of the 
release. The first publicly available EAP release for JIRA 7 is `EAP-05`. Developers 
should expect that we will only provide a few EAP releases before the final `RC` release.

***

The JIRA 7 EAP is best explored hand-in-hand with the 
[Preparing for JIRA 7 development guide](https://developer.atlassian.com/x/PYCkAQ?utm_source=DAC&utm_medium=blog&utm_campaign=jira-eap-announcement). 
The development guide contains all 
API-breaking changes in JIRA 7, suggested migration paths, 
and important new features that may affect add-on developers.

If you discover a bug or have feedback on JIRA 7, **please let us know**. 
[Create an issue in the JIRA project on jira.atlassian.com](https://jira.atlassian.com/secure/CreateIssue!default.jspa?selectedProjectId=10240) 
with the component *JIRA 7 EAP*.

<div style="text-align:center;margin: 5% auto"><button class="aui-button aui-button-primary"><a style="color:white" href="https://www.atlassian.com/software/jira/download-eap?utm_source=DAC&utm_medium=blog&utm_campaign=jira-eap-announcement">Download the JIRA 7 EAP</a></button></div>