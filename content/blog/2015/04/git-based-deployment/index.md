---
title: "Triggering Deployment with Git Commands"
date: "2015-04-10T16:00:00+07:00"
author: "ibuchanan"
categories: ["ci", "cd", "git"]
---

Deployment doesn't get any simpler than just getting the latest versions of
some files up to the production server. 
While rsync, sftp, and scp have long been the tools of the trade for such 
simple deployments, these approaches have their warts. 
Even if it is easy to recover, an remote copy that fails in the middle may 
leave a web site in an incoherent state. 
If you are already using Git to manage the files as source code, then you may 
benefit from using Git's native ability to distribute versions of files. 
While [this idea][git-instead-of-ftp] isn't all that new, there is a new 
feature of Git that makes this much easier than in past. 
Read on to learn when Git-based deployments are appropriate and how you
can use Git to deploy files.

## Simple file deployments

Here are some examples where deployment is as simple as making a copy on the
production server:

* A file-based website with just HTML, CSS, fonts, and images.
* Text configuration files like the typical contents of `/etc` on Linux.
* Interpreted source code files like JavaScript, Ruby, PHP, and Python.

This kind of simple deployment makes most sense for web sites on a 
[virtual private server][vps] (VPS), where the environment is controlled by a 
hosting provider and resources like memory are limited. 
Tools like [Capistrano][capistrano] or [Fabric][fabric] may be overkill and/or 
inappropriate for the environment. 
The one caveat is that you may have to install Git into your VPS yourself (as 
I did with my provider).

The main advantage of Git for deployment is its transactional nature. 
Consider a website where changes to the structure, content, and style.
As the files are copied to production, a visitor might get a new page that has 
a link to an updated page that hasn't yet been copied to production yet.
While the copy is in progress, the partial changes are incoherent. 
To compensate for this problem, there is a notion of 
[Blue Green Deployment][blue-green]. 
While Blue Green Deployment is essential when changes are made to multiple 
servers, it seems overkill for the kind of simple case that we're considering. 
In contrast to ftp and the like, Git will first send all of the changes to the 
production repository, then it can quickly apply all of the changes. 
If, for some reason it can't, Git will automatically roll-back. 
That's just a basic capability of Git so no special compensation is necessary.

## Push to deploy

Newer PaaS providers like Heroku and Azure offer push-to-deploy as the default 
deployment model. 
The deployment command is simple:

```bash
git push remote-server master
```

Where `remote-server` is a Git repository living on the production server. 
If the capability isn't built into your environment, you can 
[set up Git on a VPS][git-on-vps] yourself. 
However, those instructions (and dozens of others) were written before 
[Git 2.3][git-2.3], when a small but significant feature was introduced. 
Prior to Git 2.3, Git refused to modify a branch that is currently checked 
out. 
There were many work-arounds: 
[detached work tree][detached-work-tree], 
[dual repositories][dual-repositories], 
[post-receive hooks][post-receive-hook], 
and even [specialized tooling][special-tool]. 
Admittedly, these approaches cover other things. 
For example, some of these restart services upon configuration or code changes. 
Some also prevent the web server from sharing the `.git` directory but that 
can also be achieved by [configuring the web server][configure-apache].

Now, with Git 2.3, the following [configures Git][git-config] 
to override the normal behavior, making Git perform a `git reset --hard` after 
a push so that it updates the current branch.

```bash
git config receive.denyCurrentBranch updateInstead
```

## Auto-deploy the master branch with a post-push webhook

A common pattern of Git usage is to have a development branch with cutting-edge
changes and a master branch that is always kept consistent with production. 
With this pattern, it is easy to use Git to see what code is currently in
production. 
This pattern depends on automatically pushing the master branch to
production so there is never any question that master means production. 
With a Git-hosting service like Atlassian's [Bitbucket][bitbucket], the Git 
automation is a simple matter of configuring a post-push webhook.

![Bitbucket Hook](bitbucket-hook.png "Bitbucket repository administrators create Hooks under Settings -> Integrations -> Hooks.")

The trick is that [webhooks][webhook] need to be translated to a local Git
command on the target. 
For reference, I created [a simple PHP script][git-pull] to adapt the webhook 
to a `git pull`.

## Look before you leap

Although the example cases are simple, that is hardly an excuse to blindly 
deploy code into production. 
There are appropriate tools for automatically checking web sites and 
configuration files so use them. 
These Git-based deployment techniques should come after those checks, whether 
done by hand on a developing branch, or automatically by a continuous 
integration server like [Bamboo][bamboo]. 
Even if your continuous integration server makes an automatic decision to 
deploy, the advantages of the Git-based approach remain.

[git-instead-of-ftp]: https://coderwall.com/p/xczkaq/ftp-is-so-90-s-let-s-deploy-via-git-instead "Long, Kerrick (14 Jul 2012). FTP is so 90's. Let's deploy via Git instead!"
[vps]: http://en.wikipedia.org/wiki/Virtual_private_server "Wikipedia on Virtual private servers."
[capistrano]: http://capistranorb.com/ "Capistrano. A remote server automation and deployment tool written in Ruby."
[fabric]: http://www.fabfile.org/ "Fabric is a Python library and command-line tool for streamlining the use of SSH for application deployment or systems administration tasks."
[blue-green]: http://martinfowler.com/bliki/BlueGreenDeployment.html "Fowler, Martin (1 March 2010). Blue Green Deployment."
[webhook]: http://en.wikipedia.org/wiki/Webhook "Wikipedia on Webhook."
[git-pull]: https://bitbucket.org/ian_buchanan/vps-example/src/master/git-pull.php "PHP Webhook to Git Pull Adapter"
[heroku-push-to-deploy]: https://devcenter.heroku.com/articles/git "Heroku. Deploying with Git."
[git-on-vps]: https://www.digitalocean.com/community/tutorials/how-to-set-up-automatic-deployment-with-git-with-a-vps "How To Set Up Automatic Deployment with Git with a VPS."
[git-2.3]: https://raw.githubusercontent.com/git/git/master/Documentation/RelNotes/2.3.0.txt "Git v2.3 Release Notes"
[detached-work-tree]: http://toroid.org/ams/git-website-howto "Menon-Sen, Abhijit (17 Nov 2010). Using Git to manage a web site."
[dual-repositories]: http://joemaller.com/990/a-web-focused-git-workflow/ "Maller, Joe (25 Nov 2008). A web-focused Git workflow."
[post-receive-hook]: http://someguyjeremy.com/blog/quick-and-dirty-git-deployment "Harris, Jeremy (10 Oct 2011). Quick and Dirty Git Deployment."
[special-tool]: https://github.com/mislav/git-deploy "git-deploy"
[configure-apache]: http://serverfault.com/questions/128069/how-do-i-prevent-apache-from-serving-the-git-directory "How do I prevent apache from serving the .git directory?"
[bitbucket]: https://www.atlassian.com/software/bitbucket/ "Bitbucket: Git and Mercurial hosting for teams"
[git-config]: http://git-scm.com/docs/git-config#_variables "git-config variables"
[bamboo]: https://www.atlassian.com/software/bamboo "Bamboo: Continuous Integration Server"

