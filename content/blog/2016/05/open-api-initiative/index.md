---
title: "Atlassian joins Open API Initiative, open sources RADAR doc generator"
date: "2016-05-24T08:00:00+07:00"
author: "evzijst"
categories: ["apis", "opensource"]
description: "Atlassian joins of both the Open API Initiative and the Linux Foundation, and open sources RADAR"
lede: "As a backbone technology for our developer ecosystem, REST doesn't have
       an official, formal interface definition language and Atlassian is
       joining the Open API Initiative and Linux Foundation to help bring that
       about. As part of this we're open sourcing our new Open API
       documentation generator, RADAR."
---

## Developers, Developers, Developers...

Atlassian was founded by developers building tools to cater to other developers
with whom we've always had a close relationship. Very early on we opened our
products up with a plugin system allowing others to build upon and extend them,
which over time has grown into a large, thriving developer ecosystem that
we're very proud of.

##APIs and Plugin Ecosystem

Opening a product to external developers requires well-designed, stable APIs
that are thoroughly documented. The first plugins were JAR files directly
injected into our Java products. To help developers debug, we opened up the
source code and to this day all product licenses come with the full source
code.

External integrations have been supported through XML-RPC, SOAP and eventually
REST.

As cloud services gained momentum we built
[Atlassian Connect](https://connect.atlassian.com), an entirely web-based
add-on system for cloud services, open to external developers.

APIs, and in particular REST, form the backbone of Atlassian's developer
ecosystem which in turn adds substantial value to our products.

## Joining the Open API Initiative

REST gained popularity in part because of its simplicity, debuggability and
ease of use, but its lack of a commonly accepted interface description
standard has sometimes lead to inconsistent APIs and hampered code generation
efforts for rapid client development across languages.

Over time, efforts like the [Open API Initiative](https://openapis.org) have
emerged to address these limitations by defining a schema standard by which to
describe REST APIs.

As a company we're fostering an ecosystem whose success is related to the
quality and usability of its REST APIs. This means we and our external
developers have a lot to gain from an open, successful and widely-accepted
definition language for REST APIs. We've committed ourselves to actively
contributing to the standard by becoming an OAI and Linux Foundation member
organization, alongside industry leaders like Google, Microsoft, PayPal and
others.

> We're really excited to have Atlassian on board. For a technology standard
> to be successful and see broad adoption, it needs to address the industry's
> issues and involving its key players I think is a condition for achieving
> this. Atlassian's focus on interoperability and long history of successfully
> opening up its products through APIs is a huge boon for us and will
> contribute to an even stronger specification.

_Tony Tam, founder of Swagger, the foundation for the Open API Specification_

## Open Sourcing RADAR: Our API Documentation Generator

One of the things the Open API Specification facilitates is the ability to
have API documentation automatically generated. As we add Open API support to
our products, we'll use it to replace our existing, hand-crafted API
documentation.

For this we've built a custom site generator, RADAR, for Open API specs to
host our API documentation. Built on React, it offers searching, browsing and
viewing REST documentation for any product and any version of that product.
RADAR is a straight implementation of the
[current version](https://github.com/OAI/OpenAPI-Specification) of the Open API
specification, not tied to any of our own products. Because of this, RADAR
can be used by any Open API provider.

![](radar.png)

Hosted by the Linux Foundation, the Open API members do great work both
promoting and actively developing vendor-neutral open source tooling around
the specification. With RADAR we're following this model and so we are making
it available to the community under the Apache 2 license.

> Like so many others in our industry, Atlassian relies on a tremendous amount
> of open source software. In return, it has traditionally made many of its
> products freely available to our community and develops core parts of its
> products like its plugin infrastructure as open source.
> 
> We're thrilled when the industry acknowledges the important role of open
> source in ways like this, and takes responsibility by contributing back. As
> such, we're very happy to work with Atlassian as a new member organization
> of The Linux Foundation.

_Mike Woster, chief operating officer, The Linux Foundation_

We are committed to continuing its development and are keen to welcome
external contributors. The project is hosted at:
https://bitbucket.org/atlassian/radar
