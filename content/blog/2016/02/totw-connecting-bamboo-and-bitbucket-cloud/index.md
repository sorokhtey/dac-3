---
title: "How to connect Bamboo and Bitbucket Cloud"
date: "2016-02-29T12:00:00+07:00"
author: "pvandevoorde"
categories: ["totw","Bamboo","Bitbucket"]
lede: "This week I'll show you how to connect Bamboo and Bitbucket Cloud. And how this integration will help you build better software faster."
description: "Tutorial on how to connect Atlassian Bamboo and Bitbucket Cloud."
---
This week I'm going to talk about connecting Bamboo and Bitbucket Cloud.
And what this means for your software development process.

## Installation and Configuration

Start with a clean install of [Atlassian Bamboo](https://www.atlassian.com/software/bamboo/download/?utm_source=dac&utm_medium=blog&utm_campaign=totw "Download the latest version of Atlassian Bamboo."). Simply follow the [Bamboo installation instructions](https://confluence.atlassian.com/bamboo/bamboo-installation-guide-289276785.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "Atlassian Bamboo Installation Instructions.").

For demo and testing purposes the embedded database install is perfect.
Be aware that you might need to install certain prerequisites first!
And if you don't yet have a Bitbucket Cloud account you can [sign up here for a free account.](https://bitbucket.org "Atlassian Bitbucket Cloud Sign Up.")

If you don't yet have a repository you can use these instructions to clone a repo: [How to move a full Git repository.
](https://developer.atlassian.com/blog/2016/01/totw-copying-a-full-git-repo?utm_source=dac&utm_medium=blog&utm_campaign=totw "How to move a full Git repository")
Or you can choose to [fork a repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html "Forking a repo in Bitbucket Cloud.") that's available to you.

Now connect Bamboo to Bitbucket Cloud. Take a look at [Integrating Bamboo with Bitbucket Cloud](https://confluence.atlassian.com/bamboo/bitbucket-cloud-289277000.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "Integrating Bamboo with Bitbucket Cloud.") to see how you can set this up.

Here I'm setting up my Bitbucket Cloud repository in Bamboo:

![Linked Repositories](linked-repo.png "Creating a linked repository from Bitbucket Cloud in Bamboo.")

## Let's create a build plan.

Go to Bamboo and start creating a new plan, here is my configuration:

![A new build plan](create-a-plan.png "Bamboo Create Build Plan Setup.")

Due to the fact that Bitbucket Cloud and Bamboo are linked I can immediately select my repository.

The next step is adding [jobs and tasks](https://confluence.atlassian.com/bamboo/jobs-and-tasks-289277035.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "Atlassian Bamboo Jobs and Tasks") to the build plan.

For the purpose of this example I simply do a source code checkout:

![Single Task](single-task.png "Source code checkout Task in my Bamboo Build Plan.")

**Before clicking on create be sure to activate your build plan!**

Immediately a first build will be triggered and displayed in Bamboo:

![First Build](first-build.png "First build in Bamboo.")

At this time the build plan will be polling Bitbucket Cloud for changes every 180 seconds.
If you want to let commits in Bitbucket Cloud trigger builds you'll need to setup the [Bamboo Hook](https://confluence.atlassian.com/bitbucket/bamboo-service-management-278069514.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "Bamboo Hook in Bitbucket Cloud Repository") for your repository.
And you'll have to change your [build plan triggers](https://confluence.atlassian.com/bamboo/triggering-builds-289276897.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "Bamboo Build Plan Triggers").

Now the build plan will be triggered every time you commit something to the repository.

**Be aware that this will only work when your Bamboo instance is reachable from the cloud!**

You will also see the build result on each commit in Bitbucket Cloud.
And be able click through to the detailed build results in Bamboo:

![See build results in Bitbucket Cloud](build-result-in-cloud.png "Bamboo Build Results in Bitbucket CLoud.")

This is only the beginning! Let me show you what [plan branches](https://confluence.atlassian.com/bamboo/using-plan-branches-289276872.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "Bamboo Plan Branches") can do for you. Plan branches are used to represent a branch in your version control repository, with the plan branch using the same build configuration as your plan. 

Configuring plan branches is as easy as going to your build plan configuration, go to the branches tab and selecting **Create plan branches for all new branches** in the dropdown next to the label **New Branches**:

![Configuring Plan Branches in Bamboo](planned-branches.png "Configuring plan branches in Bamboo.")

Now Bamboo will create a plan for each new branch it will detect in your repository.
This build plan will be a copy of your original build plan but will have it's own logs, artifacts and triggers. Out of the box it will be triggered by the same triggers as your original build plan.
So let's create a new branch **feature-totw** to test this out.
Here is how Bamboo looks like after we've created the new branch:

![Select Plan Branches in Bamboo](branches-in-bamboo.png "Dropdown to select a plan branch in Bamboo.")

You can now select 2 branches (Master and feature-totw), this is what you see when feature-totw is selected:

![Plan first build](first-branch-build.png "Our branches first build.")

It's a complete build plan overview for this new branch.
If you have several branches at the same time you can even get a nice overview off how their builds are going in Bamboo:

![Plan Branches overview](branches-overview.png "The plan branches overview in Bamboo.")

This is very cool if you are working with a big team and using feature branches, you can immediately see which features are breaking their builds.

And this is not everything plan branches can do for you. If have been paying attention you would also have noticed the **Merging** title on the plan branches configuration tab.
This option will give you the possibility to either update your branch with the latest commits from another branch (**[Branch Updater](https://confluence.atlassian.com/display/BAMBOO059/Using+plan+branches#Usingplanbranches-Gatekeeper)**) or push your changes to another branch (**[GateKeeper](https://confluence.atlassian.com/display/BAMBOO059/Using+plan+branches#Usingplanbranches-Gatekeeper)**). Both of these actions will happen after a successful build.

![Merging Options](merging-options.png "Merging Options.")

These provide you with some extra tools to make working with feature branches so much easier.
Simply use Branch Updater to keep your feature branch up-to-date with the latest changes on Master and you will never have to worry about suddenly breaking a build when you merge your feature into Master.

Here is the result of Branch Updater in Bamboo:

![Branch Updater at work in Bamboo](branch-updater-in-bamboo.png "Branch Updater at work in Bamboo.")

And here is what you see in Bitbucket Cloud:

![Branch Updater at work in Bitbucket Cloud](branch-updater-in-bitbucket.png "Branch Updater at work in Bitbucket Cloud.")

So this is all Bamboo, but what about Bitbucket Cloud?
You already saw the build result that are available in Bitbucket Cloud and the automatic merges created by Bamboo.
But what else is there?

Pull Requests will contain build information:

![Pull Request with build information](builds-in-pull-request.png "Pull Request with build information.")

So this helps you to be sure that anything you will merge into Master will not break your Master Build and will have no impact on your releases.

Share your own tips and tricks in the comments or on Twitter, be sure to mention me: [@pvdevoor](http://twitter.com/pvdevoor "Peter Van de Voorde") or [@atlassiandev](http://twitter.com/atlassiandev "Atlassian Dev Twitter Account") !
