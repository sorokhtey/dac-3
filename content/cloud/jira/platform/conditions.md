---
aliases:
    - /jiracloud/conditions.html
    - /jiracloud/conditions.md
    - /cloud/jira/platform/conditions.html
    - /cloud/jira/platform/conditions.html
title: Conditions
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
date: "2016-10-05"
---
{{< include path="content/cloud/connect/concepts/conditions.snippet.md">}}
