---
title: Hosted Data Storage
platform: cloud
product: jiracloud
category: devguide
subcategory: block
aliases:
- /cloud/jira/platform/hosted-data-storage.html
- /cloud/jira/platform/hosted-data-storage.md
---
{{< include path="content/cloud/connect/concepts/hosted-data-storage.snippet.md">}}