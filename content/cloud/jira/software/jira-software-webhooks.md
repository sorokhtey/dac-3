---
title: JIRA Software Webhooks 
platform: cloud
product: jswcloud
category: devguide
subcategory: blocks
aliases:
- /jiracloud/jira-software-webhooks-39990316.html
- /jiracloud/jira-software-webhooks-39990316.md
confluence_id: 39990316
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39990316
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39990316
date: "2016-05-19"
---
# JIRA Software webhooks

JIRA Software webhooks are simply JIRA webhooks that are registered for JIRA Software events. JIRA Software webhooks are configured in the same way that you configure JIRA platform webhooks. You can register them via the JIRA administration console, you can add them as a post function to a workflow, you can inject variables into the webhook URL, etc. 

To learn more, read the JIRA platform documentation on webhooks: **[Webhooks]**

 

 

  [Webhooks]: /cloud/jira/platform/webhooks
