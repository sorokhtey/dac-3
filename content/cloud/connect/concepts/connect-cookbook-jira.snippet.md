
# Connect cookbook: JIRA

This cookbook is an extension of the [JavaScript API Cookbook](./javascript-api.html#cookbook),
which provides snippets of code that can be used in the clients browser to get information from the product.  

This document provides snippets specific to __JIRA__, covering:

* [Accessing a list of JIRA projects](#jira-projects)   
* [Creating a JIRA issue](#jira-create-issue)  
* [Searching JIRA with JQL](#jql-search)  

### <a name="jira-projects"></a> Accessing list of JIRA projects

Use this to retrieve a list of your JIRA projects. Depending on your projects, you might need to paginate to see 
complete results. You can do this by passing  `startAt` in the request query string.


``` javascript
AP.require('request', function(request) {
  request({
    url: '/rest/api/latest/project',
    success: function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      // dump out the response to the console
      console.log(response);
    },
    error: function() {
      console.log(arguments);
    }  
  });
});
```

### <a name="jql-search"></a> Search for an issue using JQL in JIRA

In this example you'll create a simple [JQL (JIRA query language)](https://confluence.atlassian.com/x/ghGyCg) 
query that looks for unresolved issues (`resolution = null`). The JQL query is in the `searchJql` parameter of the
request. You might need to paginate your results to get through all of them.


``` javascript
var searchJql = 'resolution = null';
AP.require('request', function(request) {
  request({
    url: '/rest/api/latest/search?jql=' + encodeURIComponent(searchJql),
    success: function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      // dump out the response to the console
      console.log(response);
    },
    error: function() {
      console.log(arguments);
    }    
  });
});
```

### <a name="jira-create-issue"></a> Creating JIRA issues

This recipe creates a new issue for an existing JIRA project. Depending on how your project is configured,
you might need to include additional fields. You might also see our [JIRA REST examples](https://developer.atlassian.com/display/JIRADEV/JIRA+REST+API+Example+-+Create+Issue) 
for reference.


``` javascript
var issueData = {
  "fields": {
    "project": { 
      "key": "TEST"
    },
    "summary": "REST ye merry gentlemen.",
    "description": "Creating of an issue using project keys and issue type names using the REST API",
    "issuetype": {
      "name": "Task"
    }
  }
};
AP.require('request', function(request) {
  request({
    url: '/rest/api/latest/issue',
    // adjust to a POST instead of a GET
    type: 'POST',
    data: JSON.stringify(issueData),
    success: function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      // dump out the response to the console
      console.log(response);
    },
    error: function() {
      console.log(arguments);
    },
    // inform the server what type of data is in the body of the HTTP POST
    contentType: "application/json"    
  });
});
```
