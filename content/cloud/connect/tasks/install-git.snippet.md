### Install Git

You can skip this step if you have Git installed.

Git is a wildly popular version control system which you'll need to complete this tutorial.
If you don't yet have git installed, you can find the
<a href="http://git-scm.com/book/en/Getting-Started-Installing-Git" target="_blank">installation instructions for your system here</a>.