<h2>Latest from the blog</h2>
<p>We always have something going on. Attend one of our events to meet other Atlassian developers.</p>

<a href="https://developer.atlassian.com/blog/"> <span class="button warning hollow tiny">Visit the blog</span></a>
