#!/usr/bin/env python

import argparse
import logging as log
import os
import re
import sys

import dac

"""
Fixes issues with code highlighting.
Firstly cleans up issues with existing <pre> tags. Then converts all <pre> tags to Hugo directives.
"""

PRE_PATTERN = re.compile(r'<pre[^>]*>(.*?)</pre>', re.I | re.S)
ANY_TAG_UNDER_PRE_PATTERN = re.compile(r'(<pre[^>]*>[^<]*)<(\w+)[^>]*>([^<]*?)</\2>([^<]*</pre>)', re.I | re.S)


def get_options():
    parser = argparse.ArgumentParser(description='Add Hugo directives for highlighting code inside HTML')
    parser.add_argument('content_dir',
                        metavar='content_dir',
                        help='the content directory to process Markdown files in')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        help='verbose: print out debug information')
    options = parser.parse_args()
    log.basicConfig(level=log.DEBUG if options.verbose else log.INFO, format=dac.LOG_FORMAT)
    return options


def cleanup_improper_formatted_code(markdown):
    """
        Cleans up any tags under the <pre> tags so they can be successfully replaced with Hugo directives.
    """
    return re.sub(ANY_TAG_UNDER_PRE_PATTERN, r'\1\3\4', markdown)


def add_highlighting_to_code_blocks(markdown):
    """
        Adds Hugo "highlight" directives to HTML code blocks that are left in the given `markdown` text
        after processing by Pandoc instead of <pre> tags.
    """
    return re.sub(PRE_PATTERN, '{{< highlight javascript >}}'+r'\1'+'{{< /highlight >}}', markdown)


def main():
    opt = get_options()
    for dirpath, dirnames, files in os.walk(opt.content_dir):
        for f in files:
            if f.endswith('.md'):
                full_file = os.path.join(dirpath, f)
                log.info("Processing file %s", full_file)
                with open(full_file) as markdown:
                    content = markdown.read()
                    content = cleanup_improper_formatted_code(content)
                    content = add_highlighting_to_code_blocks(content)
                with open(full_file, 'w+') as markdown_file:
                    print(content, end='', file=markdown_file)

if __name__ == '__main__':
    sys.exit(main())
