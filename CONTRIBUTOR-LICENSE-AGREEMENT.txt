Atlassian Pty Ltd
Developer Community Contributor License Agreement (“Agreement”)

Thank you for your interest in Atlassian Pty Ltd’s and its affiliates’ (“Atlassian”) 
Atlassian Developer Community, available at https:// developer.atlassian.com/community/ 
(the “Community”).  In order to clarify the intellectual property license granted with 
Contributions (as defined below) from any person or entity, Atlassian must have a 
Contributor License Agreement on file that has been agreed to by each Contributor (
defined below). This Agreement is for your protection as a Contributor as well as the 
protection of Atlassian and other Community users; it does not change your rights to use 
your own Contributions for any other purpose. 

Atlassian may modify this Agreement from time to time, including any referenced policies 
and other documents. Atlassian will use reasonable efforts to notify you of modifications 
(by, for example, sending an email to you, posting on our blog or through the Community). 
You may be required to click through the modified terms to show your acceptance and, in 
any event, if you continue to access or use the Community after the modification, that 
will constitute your acceptance to the modifications. If you do not agree to the modified 
terms, your sole remedy is to terminate your access and/or use of the Community.  

By signing this Agreement, clicking on the “I agree” (or similar button) that is 
presented to you along with this Agreement, or by using or accessing the Community, you 
indicate your assent to be bound by this Agreement.

1. Definitions.

“You,” “Your” or “Contributor” shall mean the copyright owner or legal entity authorized 
by the copyright owner that is making this Agreement with Atlassian. If you are an entity 
(a “Corporation”) or if you are agreeing to this Agreement not as an individual, but on 
behalf of your Corporation, then “You,” “Your” or “Contributor” means the Corporation and 
the Corporation is bound to this Agreement. For Corporations, the entity making a 
Contribution and all other entities that control, are controlled by, or are under common 
control with that entity are considered to be a single Contributor. For the purposes of 
this definition, “control” means (i) the power, direct or indirect, to cause the direction 
or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty 
percent (50%) or more of the outstanding shares, or (iii) beneficial ownership of such entity.

“Contribution” shall mean the code, documentation or other original works of authorship, 
including any modifications or additions to an existing work that is submitted by You to 
Atlassian for inclusion in any of the products or documentation owned or managed by Atlassian 
(the “Work”). For the purposes of this definition, “submitted” means any form of electronic, 
verbal or written communication sent to Atlassian or its representatives, including but not 
limited to communication on electronic mailing lists, source code control systems, and issue 
tracking systems that are managed by, or on behalf of, Atlassian for the purpose of discussing 
and improving the Work, but excluding communication that is conspicuously marked or otherwise 
designated in writing by You as “Not a Contribution.”

2. Grant of Copyright License. Subject to the terms and conditions of this Agreement, You 
hereby grant to Atlassian and to recipients of software distributed by Atlassian a perpetual, 
worldwide, non-exclusive, no-charge, royalty-free, irrevocable copyright license to reproduce, 
prepare derivative works of, publicly display, publicly perform, sublicense, and distribute 
Your Contributions and such derivative works.

3. Grant of Patent License. Subject to the terms and conditions of this Agreement, You hereby 
grant to Atlassian and to recipients of software distributed by Atlassian a perpetual, 
worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except as stated in this section) 
patent license to make, have made, use, offer to sell, sell, import, and otherwise transfer the 
Work, where such license applies only to those patent claims licensable by You that are 
necessarily infringed by Your Contribution(s) alone or by combination of Your Contribution(s) 
with the Work to which such Contribution(s) was submitted. If any entity institutes patent 
litigation against You or any other entity (including a cross-claim or counterclaim in a 
lawsuit) alleging that Your Contribution, or the Work to which You have contributed, constitutes 
direct or contributory patent infringement, then any patent licenses granted to that entity 
under this Agreement for that Contribution or Work shall terminate as of the date such 
litigation is filed.

4. You represent that You are legally entitled to grant the above license. If You are 
submitting as an individual, and if Your employer(s) has rights to intellectual property that 
You create that includes Your Contributions, You represent that You have received permission to 
make Contributions on behalf of that employer, that Your employer has waived such rights for 
Your Contributions to Atlassian, or that Your employer has executed a separate Corporate 
Contributor License Agreement with Atlassian.

5. You represent that each of Your Contributions is Your original creation (see section 7 for 
submissions on behalf of others). You represent that Your Contribution submissions include 
complete details of any third-party license or other restriction (including, but not limited to, 
related patents and trademarks) of which You are aware and which are associated with any part of 
Your Contributions.

6. You are not expected to provide support for Your Contributions, except to the extent You 
desire to provide support. You may provide support for free, for a fee, or not at all. Unless 
required by applicable law or agreed to in writing, You provide Your Contributions on an “AS 
IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied, including, 
without limitation, any warranties or conditions of TITLE, NON- INFRINGEMENT, MERCHANTABILITY, 
or FITNESS FOR A PARTICULAR PURPOSE.

7. Should you wish to submit work that is not Your original creation, You may submit it to 
Atlassian separately from any Contribution by identifying the complete details of its source 
and of any license or other restriction (including, but not limited to, related patents, 
trademarks, and license agreements) of which You are personally aware, and conspicuously 
marking the work as “Submitted on behalf of a third-party: [named here]”.

8. You agree to notify Atlassian of any facts or circumstances of which You become aware that 
would make these representations inaccurate in any respect.