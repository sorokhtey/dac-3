<VirtualHost *:80>

  ServerAdmin webmaster@atlassian.com
  ServerName developer.internal.atlassian.com
  ServerAlias developer.atlassian.com

  RedirectMatch ^/static$ https://developer.atlassian.com/static/
  RedirectMatch ^/stash$ https://developer.atlassian.com/stash/
  RedirectMatch ^/connect$ https://developer.atlassian.com/display/AC/Atlassian+Connect/

  # Redirect for ADM-58909
  RedirectMatch ^/design/* https://design.atlassian.com

  # Redirect for ADM-46394
  Redirect /connect https://developer.atlassian.com/display/AC/

  RewriteEngine on
  # Redirect old urls without versioning
  RewriteRule ^/static/connect/docs/(404\.html|assets/|concepts/|developing/|guides/|index.html|javascript/|modules/|release-notes/|resources/|rest\-apis/|scopes/)(.*) /static/connect/docs/latest/$1$2 [R,L]
  RewriteRule ^/static/connect/docs/?$ /static/connect/docs/latest/ [R,L]
  RewriteRule ^/connect$ /static/connect/docs/latest/index.html [R,L]
  RewriteRule ^/display/AC/Atlassian\+Connect$ /static/connect/docs/latest/index.html [R,L]
  RewriteRule ^/display/AC/Introduction\+to\+Atlassian\+Connect$ /static/connect/docs/latest/guides/introduction.html [R,L]
  RewriteRule ^/display/AC/Getting\+Started$ /static/connect/docs/latest/guides/getting-started.html [R,L]
  RewriteRule ^/display/AC/Hello\+World$ /static/connect/docs/latest/guides/getting-started.html [R,L]
  RewriteRule ^/display/AC/Add-on\+Descriptors$ /static/connect/docs/latest/modules/ [R,L]
  RewriteRule ^/display/AC/Pages$ /static/connect/docs/latest/modules/common/page.html [R,L]
  RewriteRule ^/display/AC/Webhooks$ /static/connect/docs/latest/modules/common/webhook.html [R,L]
  RewriteRule ^/display/AC/Development\+Loop$ /static/connect/docs/latest/developing/developing-locally.html [R,L]
  RewriteRule ^/display/AC/Installing+an\+Add-on$ /static/connect/docs/latest/developing/installing-in-ondemand.html [R,L]
  RewriteRule ^/display/AC/Add-on\+Design\+Considerations$ /static/connect/docs/latest/index.html [R,L]
  RewriteRule ^/display/AC/Licensing$ /static/connect/docs/latest/concepts/licensing.html [R,L]
  RewriteRule ^/display/AC/Listing\+Private+Add-ons$ /static/connect/docs/latest/developing/installing-in-ondemand.html [R,L]
  RewriteRule ^/display/AC/Security$ /static/connect/docs/latest/concepts/security.html [R,L]
  RewriteRule ^/display/AC/Scopes$ /static/connect/docs/latest/scopes/scopes.html [R,L]
  RewriteRule ^/display/AC/Authenticating\+with\+OAuth$ /static/connect/docs/latest/concepts/authentication.html [R,L]
  RewriteRule ^/display/AC/Permission\+Conditions$ /static/connect/docs/latest/concepts/authentication.html [R,L]
  RewriteRule ^/display/AC/Managing\+Access\+Tokens$ /static/connect/docs/latest/developing/cloud-installation.html [R,L]
  RewriteRule ^/display/AC/Selling\+on\+the\+Atlassian\+Marketplace$ /static/connect/docs/latest/developing/selling-on-marketplace.html [R,L]
  RewriteRule ^/display/AC/Developer\+Resources$ /static/connect/docs/latest/index.html [R,L]
  RewriteRule ^/display/AC/Interactive\+Descriptor\+Guide$ /static/connect/docs/latest/modules/ [R,L]
  RewriteRule ^/display/AC/Sample\+Add-ons$ /static/connect/docs/latest/resources/samples.html [R,L]
  RewriteRule ^/display/AC/REST\+API\+Browser$ /static/connect/docs/latest/rest-apis/product-api-browser.html [R,L]
  RewriteRule ^/display/AC/Atlassian\+Connect\+FAQs$ /static/connect/docs/latest/resources/faqs.html [R,L]
  RewriteRule ^/display/AC/Business\+FAQ$ /static/connect/docs/latest/resources/faqs.html [R,L]
  RewriteRule ^/display/AC/General\+FAQ$ /static/connect/docs/latest/resources/faqs.html [R,L]
  RewriteRule ^/display/AC/Technical\+FAQ$ /static/connect/docs/latest/resources/faqs.html [R,L]
  RewriteRule ^/static/connect/?$ /static/connect/docs/latest/ [R,L]
  RewriteRule ^/static/connect/docs/latest/developing/installing-in-ondemand.html$ /static/connect/docs/latest/developing/cloud-installation.html [R=301,L,NC]
  RewriteRule ^/static/connect-versions.json$ /static/connect/commands/connect-versions.json [R=301,L,NC]
  RewriteRule ^/static/connect/docs/latest/modules/(jira|confluence)/(general|admin|configure|user-profile)-page.html$ /static/connect/docs/latest/modules/common/page.html [R=301,L,NC]
  RewriteRule ^/static/connect/docs/latest/modules/(jira|confluence)/web-item.html$ /static/connect/docs/latest/modules/common/web-item.html [R=301,L,NC]
  RewriteRule ^/static/connect/docs/latest/modules/(jira|confluence)/web-panel.html$ /static/connect/docs/latest/modules/common/web-panel.html [R=301,L,NC]
  RewriteRule ^/static/connect/docs/latest/modules/(jira|confluence)/web-section.html$ /static/connect/docs/latest/modules/common/web-section.html [R=301,L,NC]
  RewriteRule ^/static/connect/docs/latest/modules/(jira|confluence)/webhook.html$ /static/connect/docs/latest/modules/common/webhook.html [R=301,L,NC]
  RewriteRule ^/static/connect/docs/latest/modules/jira/(issue|project|user-profile)-tab-panel.html$ /static/connect/docs/latest/modules/jira/tab-panel.html [R=301,L,NC]
  RewriteRule ^/static/$ /index.html [R=301,L,NC]
  RewriteRule ^/static/index.html$ /index.html [R=301,L,NC]
  RewriteRule ^/bitbucket/server/?$ /bitbucket/server/docs/latest/ [R,L]

  # PENG-4221
  RewriteRule ^/service-desk/*$ /display/jiracloud/JIRA+Service+Desk+Cloud+development [R=301,L]

  <Directory ~ "/*/static/connect/docs">
    ErrorDocument 404 /static/connect/docs/latest/404.html
  </Directory>
  SSLProxyEngine On

  # DAC reboot cut-over Migration Phase
  # https://extranet.atlassian.com/pages/viewpage.action?pageId=2989444803

  ProxyPassMatch   ^/jiracloud(.*)   https://beta-developer.atlassian.com/jiracloud$1
  ProxyPassReverse /jiracloud/     https://beta-developer.atlassian.com/jiracloud/

  ProxyPassMatch   ^/cloud/jira/(.*)  https://beta-developer.atlassian.com/cloud/jira$1
  ProxyPassReverse /cloud/jira/     https://beta-developer.atlassian.com/cloud/jira/

  ProxyPassMatch   ^/docs(.*)  https://beta-developer.atlassian.com/docs$1
  ProxyPassReverse /docs/    https://beta-developer.atlassian.com/docs/

  ProxyPassMatch   ^/community(.*)  https://beta-developer.atlassian.com/community$1
  ProxyPassReverse /community/    https://beta-developer.atlassian.com/community/

  ProxyPassMatch   ^/showcase(.*)  https://beta-developer.atlassian.com/showcase$1
  ProxyPassReverse /showcase/    https://beta-developer.atlassian.com/showcase/

  ProxyPassMatch   ^/illustrations(.*)  https://beta-developer.atlassian.com/illustrations$1
  ProxyPassReverse /illustrations/    https://beta-developer.atlassian.com/illustrations/

  ProxyPassMatch   ^/search(.*) https://beta-developer.atlassian.com/search$1
  ProxyPassReverse /search/   https://beta-developer.atlassian.com/search/

  ProxyPassMatch   /$  https://beta-developer.atlassian.com
  ProxyPassReverse ^/  https://beta-developer.atlassian.com


  # Webpack generated files
  ProxyPassMatch ^/css/dac2-site.css https://beta-developer.atlassian.com
  ProxyPassMatch ^/js/bundle.js https://beta-developer.atlassian.com
  ProxyPassMatch ^/js/foundation.bundle.js https://beta-developer.atlassian.com
  ProxyPassMatch ^/js/rssfeed.bundle.js https://beta-developer.atlassian.com

  # Pass-thru some files specific to the new site.
  ProxyPassMatch ^/font/(.*) https://beta-developer.atlassian.com/font/$1
  ProxyPassMatch ^/favicon.ico https://beta-developer.atlassian.com

  ProxyPassMatch ^/img/atlassian-logo-white.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/atlassian-logo-white.svg https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/blocks-bg.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/chat-bubble.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/clouds.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/daniel-wester-quote.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/docs.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/event-logo-aws.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/event-logo-codegeist.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/event-logo-summit.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/footer-cta-bg.afphoto https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/footer-cta-bg.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/footer-cta-bg1.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/icon-bug.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/icon-edit.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/icon-search.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/icon-tip.afdesign https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/icon-warning.svg https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/illustration-events-left.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/illustration-header-left.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/illustration-header-right.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-azure-color.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-azure-white.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-marketplace-color.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-marketplace-white.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-marketplace.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-microsoft-white.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-twilio-white.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-wittified-color.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-wittified-old.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-wittified-white.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-wittified.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-zephyr-color.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/logo-zephyr-white.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/mailbox.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/showcase-header.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/tetris-1.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/tetris-2.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/tetris-3.png https://beta-developer.atlassian.com
  ProxyPassMatch ^/img/wittified-white.png https://beta-developer.atlassian.com


  #PENG-4400
  ProxyPassMatch /bitbucket/api/2/reference/js/(.*) https://bbc-api-docs.internal.useast.atlassian.io/js/$1
  ProxyPassMatch  /bitbucket/api/2/reference/images/(.*) https://bbc-api-docs.internal.useast.atlassian.io/images/$1
  ProxyPassMatch /bitbucket/api/2/reference/(.*) https://bbc-api-docs.internal.useast.atlassian.io/docs/bitbucket/2/resource/$1
  
  # PENG-5565
  RewriteRule ^(/bitbucket/api/2/reference)$ $1/ [R=301,L]

  #PENG-4408
  ProxyPassMatch /market/api/2/reference/(.*) https://mpac-api-docs.internal.useast.atlassian.io/docs/marketplace/2/resource/$1

  Alias /static /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static
  Alias /design /opt/j2ee/domains/atlassian.com/developer-prod/static-content/design
  Alias /stash /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/stash
  Alias /bitbucket/server /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/bitbucket-server
  Alias /bitbucket /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/bitbucket
  Alias /opensource /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/opensource
  Alias /newsletter /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/newsletter

  LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" \"%{atl_cid}C\" \"%{atl_prds}C\""  combined-cookie

  ErrorLog /var/log/apache2/developer.atlassian.com-error.log
  CustomLog /var/log/apache2/developer.atlassian.com-access.log combined-cookie

  # Need this otherwise the above Alias doesn't work.
  ProxyPass /bitbucket/server !
  ProxyPass /bitbucket !
  ProxyPass /static !
  ProxyPass /design !
  ProxyPass /stash !
  ProxyPass /connect !

  # ADM-50340
  ProxyPass /index.html !
  ProxyPass /robots.txt !
  ProxyPass /help !
  ProxyPass /search !
  ProxyPass /getting-started !
  ProxyPass /nodejs !
  ProxyPass /js !
  ProxyPass /css !
  ProxyPass /.htaccess !
  ProxyPass /imgs !
  ProxyPass /blog !
  ProxyPass /articles !
  ProxyPass /google94db43b986accbc1.html !
  ProxyPass /api-reference !
  ProxyPass /opensource !
  ProxyPass /newsletter !


  Alias /index.html /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/index.html
  Alias /robots.txt /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/robots.txt
  Alias /help /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/help.html
  Alias /search /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/search.html
  Alias /getting-started /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/getting-started.html
  Alias /nodejs /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/nodejs.html
  Alias /js /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/js
  Alias /css /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/css
  Alias /.htaccess /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/.htaccess
  Alias /imgs /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/imgs
  Alias /blog /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/blog
  Alias /articles /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/articles
  Alias /google94db43b986accbc1.html /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/google94db43b986accbc1.html
  Alias /api-reference /opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/dac-homepage/current/api-reference.html

  #RewriteRule ^/$ http://developer.atlassian.com/index.html

  ProxyPass / http://127.0.0.1:8081/
  ProxyPassReverse / http://127.0.0.1:8081/

</VirtualHost>